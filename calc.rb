def suma(nu1, nu2)
  result = nu1.to_i + nu2.to_i
end

def resta(nu1, nu2)
  result = nu1.to_i - nu2.to_i
end

def multi(nu1, nu2)
  result = nu1.to_i * nu2.to_i
end

def division(nu1, nu2)
  result = nu1.to_i / nu2.to_i
end

def operaciones(operacion, nu1, nu2 )
  case operacion
    when '+'
      resp = suma(nu1, nu2)
    when '-'
      resp = resta(nu1, nu2)  
    when 'x'
      resp = multi(nu1, nu2)
    when '/'
      resp = division(nu1, nu2)
  end
  resp
end

print operaciones(ARGV[0],ARGV[1],ARGV[2])
