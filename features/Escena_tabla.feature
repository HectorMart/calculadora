
Feature: Calculadora Tabla

  Scenario Outline: Todas las operaciones
#    Given La opcion es <input> el primer valor es <input2> el segundo valor es <input3>
    Given La opcion es "<input>" el primer valor es <input2> el segundo valor es <input3>
    When Realiza la operacion 
    Then El resultado de la operacion debe ser "<output>"

      Examples:
        | input | input2 | input3 | output |
        |  +    | 2      | 2      | 4      |
        |  +    | 3      | 3      | 6      |
        |  +    | 4      | 4      | 4      |
        |  +    | 5      | 7      | 6      |
        |  -    | 10     | 2      | 8      |
        |  -    | 14     | 4      | 10     |
        |  -    | 4      | 4      | 4      |
        |  -    | 5      | 2      | 6      |
        |  x    | 2      | 2      | 4      |
        |  x    | 3      | 3      | 9      |
        |  x    | 4      | 4      | 4      |
        |  x    | 5      | 7      | 6      |
        |  /    | 10     | 2      | 5      |
        |  /    | 14     | 2      | 7      |
        |  /    | 4      | 4      | 4      |
        |  /    | 5      | 2      | 6      |