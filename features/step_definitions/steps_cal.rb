  Given /^La opcion es "([^"]*)" el primer valor es (\d+) el segundo valor es (\d+)$/ do |input, dos, tres|
  @input = input
  puts @input
  @dos = dos
  @tres = tres
end

When /^Realiza la operacion$/ do
 @output = `ruby calc.rb #{@input} #{@dos} #{@tres}`
 raise('Command failed!')  unless $?.success?
end

Then /^El resultado de la operacion debe ser "([^"]*)"$/ do |expected_output| 
 @output.should == expected_output
end

